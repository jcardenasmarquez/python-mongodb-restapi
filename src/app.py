
import string
from flask import Flask, jsonify, make_response, request, json
from PicModel import PicModel
from model import BasicTelemetry, db
from PicModel import PicModel
from datetime import datetime
from flask_cors import CORS, cross_origin

app = Flask(__name__)
cors = CORS(app)
app.config['MONGODB_SETTINGS'] = {
    'db': 'pythonmongodb',
    'host': 'localhost',
    'port': 27017
}

# db = MongoEngine()
db.init_app(app)


# HTTP Methods for the API
# GET /api/data -> to get all stored pics
# POST /api/data -> to store a new pic

@app.route('/api/pics', methods=['POST', 'GET'])
@cross_origin()
def apiData():
    if request.method == "POST":
        try:
            raw_data = request.get_json()
            raw_data["time"] = datetime.now()
            #raw_data["img"] = raw_data["img"].decode("utf-8")
            data = PicModel(**raw_data)
            data.save()
            response = {'code': '201',
                        'message': 'data added'}
            return jsonify(response), 201

        except:
            response = {'code': '403',
                        'message': 'error in the data'}
            return jsonify(response), 403

    elif request.method == "GET":
        try:
            storedPics = PicModel.objects
            response = {'code': '200',
                        'message': storedPics}
            return jsonify(response), 200

        except:
            response = {'code': '400',
                        'message': 'no data'}
            return jsonify(response), 400


@app.route('/api/pics/<id>', methods=['DELETE'])
@cross_origin()
def deletePic(id):

    if request.method == "DELETE":
        try:
            pic = PicModel.objects.get(id=id)
            pic.delete()
            storedPics = PicModel.objects
            response = {'code': '200',
                        'message': storedPics}
            return jsonify(response), 200

        except:
            response = {'code': '404',
                        'message': 'not found'}
            return jsonify(response), 404


if __name__ == "__main__":
    app.run(debug=True)
