from socket import socket
from click import pass_obj
import paho.mqtt.client as mqtt
import requests
import json

from flask import Flask, render_template, copy_current_request_context, jsonify
from flask_socketio import SocketIO, send, emit


app = Flask(__name__)
socketio = SocketIO(app, cors_allowed_origins="*")


# global_broker_address = "127.0.0.1"
# global_broker_port = 1884

global_broker_address = "classpip.upc.edu"
global_broker_port = 1884

#local_broker_address = "10.10.10.1"
local_broker_address = "localhost"
local_broker_port = 1883


API_URL = "http://localhost:5000/api/pics"

environment = "local"


@socketio.on('connect')
def test_connect(auth):
    emit('my response', {'data': 'Connected'})


@socketio.on('disconnect')
def test_disconnect():
    print('Client disconnected')


@socketio.on('connectPlatform')
def handle_message():
    client.publish("dashboard/gate/connectPlatform")


@socketio.on('armDrone')
def handle_message():
    client.publish("dashboard/autopilotService/armDrone")


@socketio.on('disarmDrone')
def handle_message():
    client.publish("dashboard/autopilotService/disarmDrone")


@socketio.on('takeOff')
def handle_message(altitude):
    client.publish("dashboard/autopilotService/takeOff", altitude)


@socketio.on('goTo')
def handle_message(latlon):
    client.publish("dashboard/autopilotService/goTo", latlon)


@socketio.on('returnToLaunch')
def handle_message():
    client.publish("dashboard/autopilotService/returnToLaunch")


@socketio.on('getTelemetry')
def handle_message():
    client.publish("dashboard/autopilotService/getDroneGroundSpeed")
    client.publish("dashboard/autopilotService/getDroneAltitude")
    client.publish("dashboard/autopilotService/getDroneHeading")
    client.publish("dashboard/autopilotService/getDronePosition")


@socketio.on('takePicture')
def handle_message():
    client.publish("dashboard/cameraService/takePicture")


@socketio.on('startLEDs')
def handle_message():
    client.publish("dashboard/LEDsService/startLEDsSequence")


@socketio.on('stopLEDs')
def handle_message():
    client.publish("dashboard/LEDsService/stopLEDsSequence")


@socketio.on('startLEDsN')
def handle_message(seconds):
    client.publish("dashboard/LEDsService/LEDsSequenceForNSeconds", seconds)


@socketio.on('getPosition')
def handle_message():
    client.publish("dashboard/autopilotService/getDronePosition")


@socketio.on('getPositionStart')
def handle_message():
    client.publish("dashboard/autopilotService/getDronePositionStart")


@socketio.on('executeFlightPlan')
def handle_message(wps):
    client.publish("dashboard/autopilotService/executeFlightPlan", wps)


@socketio.on('startVideoStream')
def handle_message():
    client.publish("videodashboard/cameraService/startVideoStream")


@socketio.on('stopVideoStream')
def handle_message():
    client.publish("videodashboard/cameraService/stopVideoStream")


@socketio.on('provaApiLocal')
def handle_message():
    client.publish(
        "videodashboard/cameraService/storePictureOnBoard")


def on_message(client, userdata, message):
    splited = message.topic.split('/')
    origin = splited[0]
    destination = splited[1]
    command = splited[2]

    if command == 'dashTelemetry':
        data = str(message.payload.decode("utf-8"))
        socketio.emit("message", data)

    if command == 'droneGroundSpeed':
        gs = str(message.payload.decode("utf-8"))
        socketio.emit("dronegroundSpeed", gs)

    if command == 'droneAltitude':
        alt = str(message.payload.decode("utf-8"))
        socketio.emit("droneAltitude", alt)

    if command == 'droneHeading':
        heading = str(message.payload.decode("utf-8"))
        socketio.emit("droneHeading", heading)

    if command == 'dronePosition':
        pos = str(message.payload.decode("utf-8"))
        socketio.emit("dronePosition", pos)

    if command == 'picture':
        picture = str(message.payload.decode("utf-8"))
        socketio.emit("picture", picture)

    if command == 'storedPicture':
        body = str(message.payload.decode("utf-8")).split("-")
        requests.post(API_URL, json={'time': "",
                      'coord': body[1], 'img': body[0]})

    if command == 'videoFrame':
        videoframe = str(message.payload.decode("utf-8"))
        socketio.emit("video", videoframe)


client = mqtt.Client("Data service")
client.on_message = on_message
client.username_pw_set(username='ecosystem', password='eco1342.')

if (environment == "local"):
    client.connect(local_broker_address, local_broker_port)
else:
    client.connect(global_broker_address, global_broker_port)
client.loop_start()

print('Waiting commnads')
#
client.subscribe('proves/dataService/dashTelemetry')
client.subscribe('+/dashboard/#')
client.subscribe('+/videodashboard/#')

# port4000
if __name__ == '__main__':
    socketio.run(app,  port=4000)
    #socketio.run(app, host="192.168.1.236", port=4000)
