from flask import Flask
from flask_socketio import SocketIO

import paho.mqtt.client as mqtt

app = Flask(__name__)
socketio = SocketIO(app, cors_allowed_origins="*")

broker_address = "localhost"
broker_port = 1883


@socketio.on('connectPlatform')
def handle_message():
    client.publish("dashboard/gate/connectPlatform")


def on_message(client, userdata, message):
    splited = message.topic.split('/')
    command = splited[2]

    if command == 'dronePosition':
        pos = str(message.payload.decode("utf-8"))
        socketio.emit("dronePosition", pos)


client = mqtt.Client("Tutorial backend")
client.on_message = on_message
client.connect(broker_address, broker_port)
client.loop_start()

client.subscribe("+/dashboard/#")

if __name__ == '__main__':
    socketio.run(app, port=4000)
    #socketio.run(app, host="192.168.1.20", port=4000)
