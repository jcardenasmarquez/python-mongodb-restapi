from flask_mongoengine import MongoEngine

db = MongoEngine()


class PicModel(db.Document):
    time = db.DateTimeField()
    coord = db.StringField()
    img = db.StringField()

    def to_json(self):
        # Convers teh document to JSON
        return {
            "time": self.time,
            "coord": self.coord,
            "img": self.img
        }
