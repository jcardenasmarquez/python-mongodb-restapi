from flask_mongoengine import MongoEngine

db = MongoEngine()


class BasicTelemetry(db.Document):
    time = db.DateTimeField()
    lat = db.StringField()
    lon = db.StringField()

    def to_json(self):
        # Convers teh document to JSON
        return {
            "time": self.time,
            "lat": self.lat,
            "lon": self.lon
        }
